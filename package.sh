#!/usr/bin/env bash

mkdir -p target/

tar czf ./target/nara.tar.gz     ./nara/
tar czf ./target/tantalus.tar.gz ./tantalus/
tar czf ./target/gouyen.tar.gz   ./gouyen/
