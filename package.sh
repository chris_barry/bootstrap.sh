#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# package.sh - Package scripts as a tar.gz
# Copyright (C) 2025 Chris Barry <chris@barry.im>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

mkdir -p target/

tar czf ./target/nara.tar.gz     ./nara/
tar czf ./target/tantalus.tar.gz ./tantalus/
tar czf ./target/gouyen.tar.gz   ./gouyen/
