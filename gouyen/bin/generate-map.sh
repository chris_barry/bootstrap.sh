#!/usr/bin/env bash

python3 /opt/Minecraft-Overviewer/overviewer.py \
        --config /opt/bootstrap.sh/gouyen/config/minecraft-map.py && \
python3 /opt/Minecraft-Overviewer/overviewer.py \
        --config /opt/bootstrap.sh/gouyen/config/minecraft-map.py \
        --genpoi && \
rsync \
  --archive \
  --verbose \
  --update \
  --recursive \
  --delete \
  --rsh "ssh -o StrictHostKeyChecking=no -i /home/chris/.ssh/id_ed25519" \
  /opt/minecraft/map chris@marion.barry.im:/opt/minecraft/