#!/usr/bin/env bash
#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# bootstrap.sh - Bootstrap new installation
# Copyright (C) 2025 Chris Barry <chris@barry.im>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e
export DEBIAN_FRONTEND=noninteractive

##### START SSHD #####
echo '#Port 44039'               > /etc/ssh/sshd_config.d/00-port.conf
echo 'PasswordAuthentication no' > /etc/ssh/sshd_config.d/01-password-auth.conf
systemctl restart sshd
##### END SSHD   #####

##### START DOCKER #####
apt-get install -qy \
  podman-docker

##### END DOCKER   #####

##### START Certbot #####
apt-get install -qy \
  certbot \
  python3-certbot-nginx

systemctl enable certbot.timer
systemctl start certbot.timer
##### END Certbot   #####

##### START WEB #####
apt-get install -qy \
  nginx \
  hugo

ln -s /opt/bootstrap.sh/gouyen/systemd/barryim.service /etc/systemd/system/barryim.service
ln -s /opt/bootstrap.sh/gouyen/systemd/barryim.timer   /etc/systemd/system/barryim.timer

ln -s /opt/bootstrap.sh/gouyen/systemd/nealcat.service /etc/systemd/system/nealcat.service
ln -s /opt/bootstrap.sh/gouyen/systemd/nealcat.timer   /etc/systemd/system/nealcat.timer

systemctl daemon-reload

mkdir -p /etc/nginx/tls/
openssl dhparam -out /etc/nginx/tls/dhparam.pem 2048

rm /etc/nginx/sites-enabled/default
ln -s /opt/bootstrap.sh/gouyen/config/barry.im.conf /etc/nginx/sites-enabled/barry.im.conf
ln -s /opt/bootstrap.sh/gouyen/config/neal.cat.conf /etc/nginx/sites-enabled/neal.cat.conf

mkdir -p /srv/www.barry.im
git -C /opt/www.barry.im         clone https://gitlab.com/chris_barry/barry.im.git
git -C /opt/www.barry.im/themes clone https://github.com/yihui/hugo-xmin.git

mkdir -p /srv/www.neal.cat
git -C /opt/neal.cat clone https://gitlab.com/chris_barry/neal.cat.git

systemctl enable barryim
systemctl enable barryim.timer
systemctl start  barryim
systemctl start  barryim.timer
systemctl enable nealcat
systemctl enable nealcat.timer
systemctl start  nealcat
systemctl start  nealcat.timer
##### END WEB   #####

##### START I2P #####
apt-get install -qy \
  apt-transport-https \
  lsb-release \
  curl

echo "deb [signed-by=/usr/share/keyrings/i2p-archive-keyring.gpg] https://deb.i2p.net/ $(lsb_release -sc) main" \
| tee /etc/apt/sources.list.d/i2p.list

curl -o "/usr/share/keyrings/i2p-archive-keyring.gpg" https://geti2p.net/_static/i2p-archive-keyring.gpg

apt-get update
apt-get install -qy \
  i2p \
  i2p-keyring \
  rsync

systemctl stop i2p
cat >>/var/lib/i2p/i2p-config/router.config << EOF
router.sharePercentage=70
i2np.udp.port=11331
i2np.udp.enable=true
i2np.udp.internalPort=11331
i2np.udp.ipv6=only
i2np.ntcp.port=11331
i2np.ntcp.ipv6=only
i2np.bandwidth.inboundBurstKBytes=48400
i2np.bandwidth.inboundBurstKBytesPerSecond=2420
i2np.bandwidth.inboundKBytesPerSecond=2200
i2np.bandwidth.outboundBurstKBytes=48400
i2np.bandwidth.outboundBurstKBytesPerSecond=2420
i2np.bandwidth.outboundKBytesPerSecond=2200
EOF

systemctl start i2p

mkdir -p /usr/share/i2p/ssh
echo "reseed.onion.im ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH3wK9R8uX155nGLEFDT5zS64rDk0uj+4NQBxwWL/++U" >/usr/share/i2p/ssh/known_hosts
ssh-keygen -t ed25519 -f /usr/share/i2p/ssh/id_ed25519
chown -R i2psvc:i2psvc /usr/share/i2p/ssh/

ln -s /opt/bootstrap.sh/gouyen/systemd/reseed.service /etc/systemd/system/reseed.service
ln -s /opt/bootstrap.sh/gouyen/systemd/reseed.timer   /etc/systemd/system/reseed.timer

systemctl daemon-reload

systemctl enable reseed
systemctl enable reseed.timer
systemctl start reseed
systemctl start reseed.timer
##### END I2P   #####

##### START SYNCTHING #####
apt-get install -qy \
  syncthing
systemctl enable syncthing@chris.service
systemctl start syncthing@chris.service
##### END SYNCTHING   #####

##### START MINECRAFT GENERAL #####
mkdir -p /opt/minecraft/bin
mkdir -p /opt/minecraft/data
mkdir -p /opt/minecraft/map
##### END   MINECRAFT GENERAL #####

##### START MINECRAFT #####
chown -R minecraft:minecraft /opt/minecraft

ln -s /opt/bootstrap.sh/gouyen/bin/minecraft/start.sh /opt/minecraft/bin/start.sh
ln -s /opt/bootstrap.sh/gouyen/systemd/minecraft.service /etc/systemd/system/minecraft.service
ln -s /opt/bootstrap.sh/gouyen/config/minecraft.server.properties /opt/minecraft/data/server.properties
systemctl daemon-reload
systemctl enable minecraft
##### END   MINECRAFT #####

##### START NETWORKING #####
# Enable ipv4 and ipv6

cat >/etc/network/interfaces.d/enp0s31f6 << EOF
allow-hotplug enp0s31f6
iface enp0s31f6 inet dhcp
iface enp0s31f6 inet6 dhcp
EOF

cat >/etc/network/interfaces.d/enp4s0 << EOF
allow-hotplug enp4s0
iface enp4s0 inet dhcp
iface enp4s0 inet6 dhcp
EOF
##### END   NETWORKING #####

##### START STUFF #####
apt-get install -qy \
  cryptsetup \
  htop \
  tmux
##### END   STUFF #####

# 0. site - Check DNS records
# 0. site - Set up certbot from backup configs
# 0. i2p - configure port
# 0. i2p - configure speed
# 0. i2p - configure ipv6 only
# 0. reseed - copy gouyen.barry.im:/usr/share/i2p/ssh/id_ed25519.pub to reseed.onion.im:/home/reseed/.ssh/authorized_keys
# 0. router - configure new ipv4/ipv6 addresses
# 0. minecraft - copy backups neal.cat
# 0. minecraft - copy backups thepapertrail.xyz
