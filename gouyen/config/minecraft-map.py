#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
#
# minecraft-map.py - configurations for generating minecraft map
# Copyright (C) 2025 Chris Barry <chris@barry.im>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

global escape
from html import escape
import datetime

def signFilter(poi):
    if poi["id"] == "Sign" or poi["id"] == "minecraft:sign":
        return escape("\n".join([poi["Text1"], poi["Text2"], poi["Text3"], poi["Text4"]]))

imgformat = "webp"
texturepath = "/opt/minecraft/textures-1.17.1.jar"
rendermode = "lighting"
outputdir = "/opt/minecraft/map"
title = datetime.datetime.now().isoformat()
worlds["world"] = "/opt/minecraft/data/world"

renders["normalrender"] = {
  "world": "world",
  "title": "craft.neal.cat:normal",
  "rendermode": normal,
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"

}

#renders["caves"] = {
#  "world": "world",
#  "title": "craft.neal.cat:caves",
#  "rendermode": cave,
#  "markers": [dict(name="signs", filterFunction=signFilter)],
#  "title": title,
#  "imgformat": "webp"
#}

renders["survivalnether"] = {
  "world": "world",
  "title": "craft.neal.cat:nether",
  "rendermode": nether,
  "dimension": "nether",
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"
}

renders["end"] = {
  "world": "world",
  "title": "craft.neal.cat:end",
  "rendermode": normal,
  "dimension": "end",
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"
}

