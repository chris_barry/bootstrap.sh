global escape
from html import escape
import datetime

def signFilter(poi):
    if poi["id"] == "Sign" or poi["id"] == "minecraft:sign":
        return escape("\n".join([poi["Text1"], poi["Text2"], poi["Text3"], poi["Text4"]]))

imgformat = "webp"
texturepath = "/opt/minecraft/textures-1.17.1.jar"
rendermode = "lighting"
outputdir = "/opt/minecraft/map"
title = datetime.datetime.now().isoformat()
worlds["world"] = "/opt/minecraft/data/world"

renders["normalrender"] = {
  "world": "world",
  "title": "craft.neal.cat:normal",
  "rendermode": normal,
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"

}

#renders["caves"] = {
#  "world": "world",
#  "title": "craft.neal.cat:caves",
#  "rendermode": cave,
#  "markers": [dict(name="signs", filterFunction=signFilter)],
#  "title": title,
#  "imgformat": "webp"
#}

renders["survivalnether"] = {
  "world": "world",
  "title": "craft.neal.cat:nether",
  "rendermode": nether,
  "dimension": "nether",
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"
}

renders["end"] = {
  "world": "world",
  "title": "craft.neal.cat:end",
  "rendermode": normal,
  "dimension": "end",
  "markers": [dict(name="signs", filterFunction=signFilter)],
  "title": title,
  "imgformat": "webp"
}
