#!/usr/bin/env bash

set -e
export DEBIAN_FRONTEND=noninteractive

##### START MINECRAFT #####
TEMP=$(mktemp -d)
wget -O "$TEMP"/minecraft.deb https://launcher.mojang.com/download/Minecraft.deb
apt-get install -qy "$TEMP"/minecraft.deb
rm "$TEMP"
##### END MINECRAFT   #####

##### START I2P #####
echo "deb https://deb.i2p2.de/ buster main" >/etc/apt/sources.list.d/i2p.list
echo "deb-src https://deb.i2p2.de/ buster main" >>/etc/apt/sources.list.d/i2p.list
wget -O - https://geti2p.net/_static/i2p-debian-repo.key.asc | apt
apt-get update
apt-get install -qy \
  i2p \
  i2p-router
systemctl stop i2p
systemctl disable i2p
echo "i2np.laptopMode=true" >>/usr/share/i2p/router.config
##### END I2P #####

##### START SIGNAL #####
wget -O - https://updates.signal.org/desktop/apt/keys.asc | apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" >/etc/apt/sources.list.d/signal-xenial.list
apt-get update
apt-get install -qy \
  signal-desktop
##### END SIGNAL   #####

##### START STUFF I JUST LIKE #####
apt-get install -qy \
  apt-listchanges \
  apt-transport-https \
  apparmor \
  apparmor-utils \
  awscli \
  laptop-mode-tools \
  git \
  gnupg \
  htop \
  firefox-esr \
  rsync \
  syncthing \
  powermgmt-base \
  sudo \
  tomb \
  unattended-upgrades \
  vlc

systemctl enable laptop-mode
systemctl start laptop-mode

systemctl enable syncthing@chris.service
systemctl start syncthing@chris.service

git clone git@gitlab.com:chris_barry/bin.git /home/chris/bin

##### END STUFF I JUST LIKE #####
echo "set up ssh keys now"
echo "run powertop"
