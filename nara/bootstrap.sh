#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# bootstrap.sh - Bootstrap new installation
# Copyright (C) 2025 Chris Barry <chris@barry.im>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e
export DEBIAN_FRONTEND=noninteractive

##### START MINECRAFT #####
TEMP=$(mktemp -d)
wget -O "$TEMP"/minecraft.deb https://launcher.mojang.com/download/Minecraft.deb
apt-get install -qy "$TEMP"/minecraft.deb
rm "$TEMP"
##### END MINECRAFT   #####

##### START I2P #####
echo "deb https://deb.i2p2.de/ buster main" >/etc/apt/sources.list.d/i2p.list
echo "deb-src https://deb.i2p2.de/ buster main" >>/etc/apt/sources.list.d/i2p.list
wget -O - https://geti2p.net/_static/i2p-debian-repo.key.asc | apt
apt-get update
apt-get install -qy \
  i2p \
  i2p-router
systemctl stop i2p
systemctl disable i2p
echo "i2np.laptopMode=true" >>/usr/share/i2p/router.config
##### END I2P #####

##### START SIGNAL #####
wget -O - https://updates.signal.org/desktop/apt/keys.asc | apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" >/etc/apt/sources.list.d/signal-xenial.list
apt-get update
apt-get install -qy \
  signal-desktop
##### END SIGNAL   #####

##### START STUFF I JUST LIKE #####
apt-get install -qy \
  apt-listchanges \
  apt-transport-https \
  apparmor \
  apparmor-utils \
  awscli \
  laptop-mode-tools \
  git \
  gnupg \
  htop \
  firefox-esr \
  rsync \
  syncthing \
  powermgmt-base \
  sudo \
  tomb \
  unattended-upgrades \
  vlc

systemctl enable laptop-mode
systemctl start laptop-mode

systemctl enable syncthing@chris.service
systemctl start syncthing@chris.service

git clone git@gitlab.com:chris_barry/bin.git /home/chris/bin

##### END STUFF I JUST LIKE #####
echo "set up ssh keys now"
echo "run powertop"
