# bootstrap

A bunch of scripts I use to remake machines.
Please don't make pull requests my bad code should stay bad.

## Generic
```
server
\-> bootstrap.sh
\-> config/
\--> various configs
\-> systemd/
\--> systemd configs
```
### Ports
- 123/TCP

## Coconut
```
./coconut/bootstrap.sh
```

## Tantalus
```
./tantalus/bootstrap.sh
```
### Ports
- 443/TCP 

## Nara
```
./nara/bootstrap.sh
```

## Marion
### Install
```
$ sudo -i
# apt install git
# mkdir -p /opt/bootstrap
# git clone https://gitlab.com/chris_barry/bootstrap.sh.git /opt/bootstrap
# cd /opt/bootstrap/marion
# sh boostrap.sh
```
### Ports

-    80/TCP -     http
-   443/TCP -     https
- 44039/TCP -     ssh
- 22000/TCP -     syncthing - upnp
- 25565/TCP -     minecraft
- ?????/TCP+UCP - i2p - upnp

## License
This project is licensed under the **GNU General Public License v3.0**.
You are free to use, modify, and distribute it under the terms of the GPLv3 or later.

See the [LICENSE](LICENSE) file for details.

For more information, visit: [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

