#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# bootstrap.sh - Bootstrap new installation
# Copyright (C) 2025 Chris Barry <chris@barry.im>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e
export DEBIAN_FRONTEND=noninteractive

# LC Stuff
sudo locale-gen en_US.UTF-8
sudo ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Make user for me
sudo adduser --system --shell /bin/bash --home /home/chris --group chris
sudo usermod -aG sudo chris gpio www-data

sudo su - chris -c 'mkdir -p /home/chris/.ssh/'
sudo su - chris -c 'echo ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDH3SxL1ULrQ41c+Z1v8EITpcJM58qU4cYr5xE+u/doK chris@nara.barry.im > /home/chris/.ssh/authorized_keys'
echo '%sudo ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/10-chris
echo '' | sudo tee /home/pi/.ssh/authorized_keys

# Switch to me and remove pi user
su chris
sudo userdel -r pi

# Change hostname
echo "tantalus"| sudo tee /etc/hostname
sudo sed -i 's/raspberrypi/tantalus/g' /etc/hosts

# ntp
sudo apt-get install -qy \
    npt 

# Unattended upgrades
sudo apt-get install -qy \
    apt-listchanges \
    unattended-upgrades

echo unattended-upgrades unattended-upgrades/enable_auto_updates boolean true | sudo tee debconf-set-selections
sudo dpkg-reconfigure -f noninteractive unattended-upgrades

# Stuff I just like
sudo apt-get install -qy \
    git \
    htop \
    tmux \
    vim

# Install stuff for servo
sudo apt-get install -qy \
    nginx \
    python3-flask \
    python3-gpiozero \
    python3-pigpio \
    sqlite3 \
    uwsgi \
    uwsgi-plugin-python3

sudo mkdir -p /usr/local/keys/;
sudo mkdir -p /etc/nginx/tls/
sudo openssl dhparam -out /etc/nginx/tls/dhparam.pem 2048

openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3650 \
            -nodes \
            -out /usr/local/keys/feeder.crt \
            -keyout /usr/local/keys/feeder.key \
            -subj "/C=SI/ST=N/L=N/O=N/OU=N/CN=tantalus.barry.im"

sudo systemctl enable pigpiod
sudo systemctl start pigpiod

sudo update-alternatives --set uwsgi /usr/bin/uwsgi_python3

ln -s /opt/bootstrap.sh/tantalus/systemd/feeder.service /etc/systemd/system/feeder.service
sudo systemctl start feeder
sudo systemctl enable feeder
